class ChangeRalationshipsBetweenAssetsAndProjects < ActiveRecord::Migration[5.0]
  def change
    remove_column :assets, :application_id, :integer
    add_reference :assets, :project, foreign_key: true    
  end
end
