class CreateSizes < ActiveRecord::Migration[5.0]
  def change
    create_table :sizes do |t|
      t.references :asset, foreign_key: true
      t.text :name
      t.integer :height
      t.integer :width
      t.boolean :correct

      t.timestamps
    end
  end
end
