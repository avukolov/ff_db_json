class AddSizesHstoreToAsset < ActiveRecord::Migration[5.0]
  def change
    enable_extension "hstore"
    add_column :assets, :sizes, :hstore
    add_index :assets, :sizes, using: :gist
  end
end
