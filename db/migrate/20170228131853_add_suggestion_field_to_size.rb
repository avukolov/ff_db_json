class AddSuggestionFieldToSize < ActiveRecord::Migration[5.0]
  def change
    add_column :sizes, :hint, :text
  end
end
