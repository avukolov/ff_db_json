class AddRowsToSpreadsheet < ActiveRecord::Migration[5.0]
  def change
    add_column :spreadsheets, :rows, :string, array: true, default: '{}'
  end
end
