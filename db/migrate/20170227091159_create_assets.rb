class CreateAssets < ActiveRecord::Migration[5.0]
  def change
    create_table :assets do |t|
      t.references :application, foreign_key: true
      t.text :name

      t.timestamps
    end
  end
end
