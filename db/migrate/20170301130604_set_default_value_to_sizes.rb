class SetDefaultValueToSizes < ActiveRecord::Migration[5.0]
  def change
    change_column :assets, :sizes, :hstore, default: '', null: false
  end
end
