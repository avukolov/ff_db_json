require 'csv'
require 'ruby-progressbar'

puts "Deleting all records"
Project.destroy_all
puts "All records were deleted"
puts "Start parse csv"
csv_text = File.read(Rails.root.join('tmp', 'out.csv'))

raw_data = {}
projects = CSV.parse(csv_text, :headers => false, :encoding => 'UTF-8').group_by{|project,name,width,height,type| project.split('/').second}
projects.each do |project_name,params|
  asset = {}
  asset = params.group_by{ |project, name, width, height, type| name.lstrip }
  raw_data[project_name] = asset
end

raw_data.each do |project_name, asset_data|
  project = Project.find_or_create_by(title: project_name)
  asset_data.each do |asset_name, asset_params|
    if asset_name.match('png|jpg|jpeg')
      asset = project.assets.find_or_create_by(name: asset_name.lstrip.gsub(/.png|.jpeg/,''))
      asset_params.each do |size|
        size_name = size[0].split('/').last

        size_hash = {
          width: size[2].lstrip.to_i,
          height: size[3].lstrip.to_i,
          correct: false,
          hint: ""
        }

        asset.sizes[size_name] = size_hash
      end
      asset.save!
    end
  end
end


#check if size of asset is correct
Asset.all.each do |asset|
  asset.calculate_correct_size
end
