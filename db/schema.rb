# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170302153023) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"

  create_table "applications", force: :cascade do |t|
    t.text     "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "assets", force: :cascade do |t|
    t.text     "name"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "project_id"
    t.hstore   "sizes",      default: {}, null: false
    t.index ["project_id"], name: "index_assets_on_project_id", using: :btree
    t.index ["sizes"], name: "index_assets_on_sizes", using: :gist
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",         default: 0, null: false
    t.integer  "attempts",         default: 0, null: false
    t.text     "handler",                      null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "progress_stage"
    t.integer  "progress_current", default: 0
    t.integer  "progress_max",     default: 0
    t.index ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree
  end

  create_table "projects", force: :cascade do |t|
    t.text     "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sizes", force: :cascade do |t|
    t.integer  "asset_id"
    t.text     "name"
    t.integer  "height"
    t.integer  "width"
    t.boolean  "correct"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text     "hint"
    t.index ["asset_id"], name: "index_sizes_on_asset_id", using: :btree
  end

  create_table "spreadsheets", force: :cascade do |t|
    t.text     "title"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "rows",       default: [],              array: true
  end

  create_table "words", force: :cascade do |t|
    t.text     "word"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "assets", "projects"
  add_foreign_key "sizes", "assets"
end
