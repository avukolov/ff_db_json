Rails.application.routes.draw do
  resources :projects
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root to: "homepage#index"

  get 'db', to: "homepage#show"
  get 'download', to: "homepage#download_json"
  get 'seed_spreadsheets', to: "settings#seed_spreadsheets"
  get 'settings', to: "settings#index"
  post 'upload', to: "settings#upload", as: :upload
  get 'seed_assets', to: "settings#seed_assets", as: :seed_assets

end
