class Project < ApplicationRecord
  has_many :assets, dependent: :destroy
end
