class Asset < ApplicationRecord
  belongs_to :project
  serialize :sizes, ActiveRecord::Coders::NestedHstore

  names = ["drawable-mdpi", "drawable-hdpi", "drawable-xhdpi", "drawable-xxhdpi", "drawable-xxxhdpi", "drawable-sw600dp-ldpi", "drawable-sw600dp-mdpi", "drawable-sw600dp-hdpi", "drawable-sw600dp-xhdpi", "drawable-sw720dp-ldpi", "drawable-sw720dp-mdpi", "drawable-sw720dp-hdpi", "drawable-sw720dp-xhdpi"]

  def calculate_correct_size
    ratio = {
      "drawable-mdpi" => 1, "drawable-hdpi" => 1.5, "drawable-xhdpi" => 2, "drawable-xxhdpi" => 3, "drawable-xxxhdpi" => 4, "drawable-sw600dp-ldpi" => 0.75, "drawable-sw600dp-mdpi" => 1, "drawable-sw600dp-hdpi" => 1.5, "drawable-sw600dp-xhdpi" => 2, "drawable-sw720dp-ldpi" => 0.75, "drawable-sw720dp-mdpi" => 1, "drawable-sw720dp-hdpi" => 1.5, "drawable-sw720dp-xhdpi" => 2
    }

    unless sizes["drawable-mdpi"].nil?
      sizes.each do |name, properties|
        if ratio.keys.include?(name)
          #for phones
          if name.match("drawable-[a-z]{3,}")
            mdpi_w = sizes["drawable-mdpi"]["width"]
            mdpi_h = sizes["drawable-mdpi"]["height"]
          #for tablets 7"
          elsif name.match("drawable-sw600dp")
            mdpi_w = sizes["drawable-sw600dp-mdpi"].nil? ? sizes["drawable-mdpi"]["width"] : sizes["drawable-sw600dp-mdpi"]["width"]
            mdpi_h = sizes["drawable-sw600dp-mdpi"].nil? ? sizes["drawable-mdpi"]["height"] : sizes["drawable-sw600dp-mdpi"]["height"]
          #for tablets 10"
          elsif name.match("drawable-sw720dp")
            mdpi_w = sizes["drawable-sw720dp-mdpi"].nil? ? sizes["drawable-mdpi"]["width"] : sizes["drawable-sw720dp-mdpi"]["width"]
            mdpi_h = sizes["drawable-sw720dp-mdpi"].nil? ? sizes["drawable-mdpi"]["height"] : sizes["drawable-sw720dp-mdpi"]["height"]
          end
        properties['correct'] = is_correct?(properties["width"], properties["height"], mdpi_w, mdpi_h, ratio[name])
        end
      end
    save!
    end
  end


  def is_correct?(asset_w, asset_h, mdpi_w, mdpi_h, ratio)
    correct = [
      (mdpi_w * ratio - mdpi_w * 0.15 < asset_w),
      (asset_w < mdpi_w * ratio + mdpi_w * 0.15),
      (mdpi_h * ratio - mdpi_h * 0.15 < asset_h),
      (asset_h < mdpi_h * ratio + mdpi_h * 0.15)
    ].all?
  end

end
=begin
size.update(correct: false, hint: "-10%: #{mdpi_w * ratio - mdpi_w * 0.1}x#{mdpi_h * ratio - mdpi_h * 0.1}
0: #{mdpi_w * ratio}x#{mdpi_h * ratio}
+10%: #{mdpi_w * ratio + mdpi_w * 0.1}x#{mdpi_h * ratio + mdpi_h * 0.1}")
puts 'hint was added'
=end
