class SettingsController < ApplicationController
  require 'csv'
  require 'dropbox_sdk'
  before_action :dropbox_auth, only: [:index, :upload]

  def index
    @last_upload = @dropbox_client.search('/','.csv').last["modified"].to_time.in_time_zone("Kyiv").strftime("%a, %d %b %Y %T")
  end

  def upload
    #upload file to Dropbox
    @dropbox_client.put_file(params[:upload][:file].original_filename, params[:upload][:file].tempfile)
    flash[:notice] = "File uploaded"
    redirect_to "/settings"
  end

  def seed_spreadsheets
    SeedSpreadsheetsJob.perform_later
    redirect_to "/settings"
  end

  def seed_assets
    SeedAssetsJob.perform_later
    redirect_to root_path
  end

  private

  def dropbox_auth
    access_token = 'LeBsNFZ4OQkAAAAAAAArRMJWrd5Ag2K1SI28kEyzgbPKslbCufOAjXp73RArOqkI'
    @dropbox_client = DropboxClient.new(access_token)
  end

end
