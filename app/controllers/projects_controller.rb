class ProjectsController < ApplicationController

  def index
    @projects = Project.all
  end

  def show
    @project = Project.find(params[:id])
    @headers = %w(name mdpi hdpi xhdpi xxhdpi xxxhdpi sw600dp-ldpi sw600dp-mdpi sw600dp-hdpi sw600dp-xhdpi sw720dp-ldpi sw720dp-mdpi sw720dp-hdpi sw720dp-xhdpi)
  end

end
