class HomepageController < ApplicationController
  before_action :set_spreadsheet, :except => [:index, :refresh_db]

  def index
    @spreadsheets = Spreadsheet.all
  end

  def show
    @spreadsheet
  end

  def download_json
    @spreadsheet

    source = @spreadsheet.rows
    headers = source[0]

    to_json = []
    source[1..source.count].each do |row|
      hash = {}
      row.each_with_index do |v, i|
        hash[headers[i].to_sym] = v
      end
      to_json.push(hash)
    end

    filename = "#{@spreadsheet.title}_#{Date.today.strftime('%m_%d_%Y')}"
    File.open("#{Rails.root}/public/#{filename}.json","w") do |file|
      file.puts JSON.pretty_generate('rows' => to_json)
    end

    send_file "#{Rails.root}/public/#{filename}.json"
  end

  private
  def set_spreadsheet
    @spreadsheet = Spreadsheet.find(params[:id])
  end
end
