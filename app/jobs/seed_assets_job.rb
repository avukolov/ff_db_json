class SeedAssetsJob < ApplicationJob
  require 'csv'
  require 'dropbox_sdk'
  queue_as :default

  def perform(*args)
    #connect_to_Dropbox
    access_token = 'LeBsNFZ4OQkAAAAAAAArRMJWrd5Ag2K1SI28kEyzgbPKslbCufOAjXp73RArOqkI'
    @dropbox_client = DropboxClient.new(access_token)

    #Delete all exist Projects
    Project.destroy_all

    #find lates uploaded csv to Dropbox
    path_to_csv = @dropbox_client.search('/','.csv').last["path"]

    #Seed db for Assets
    raw_data = {}
    projects = CSV.parse(@dropbox_client.get_file(path_to_csv), :headers => false, :encoding => 'UTF-8').group_by{|project,name,width,height,type| project.split('/').second}
    projects.each do |project_name,params|
      asset = {}
      asset = params.group_by{ |project, name, width, height, type| name.lstrip }
      raw_data[project_name] = asset
    end

    raw_data.each do |project_name, asset_data|
      project = Project.find_or_create_by(title: project_name)
      asset_data.each do |asset_name, asset_params|
        if asset_name.match('png|jpg|jpeg')
          asset = project.assets.find_or_create_by(name: asset_name.lstrip.gsub(/.png|.jpg|.jpeg/,''))
          asset_params.each do |size|
            size_name = size[0].split('/').last

            size_hash = {
              width: size[2].lstrip.to_i,
              height: size[3].lstrip.to_i,
              correct: false,
              hint: ""
            }

            asset.sizes[size_name] = size_hash
          end
          asset.save!
        end
      end
    end

    Asset.all.each do |asset|
      asset.calculate_correct_size
    end

  end
end
