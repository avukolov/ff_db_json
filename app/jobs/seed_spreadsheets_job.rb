class SeedSpreadsheetsJob < ApplicationJob
  queue_as :default

  def perform(*args)
    #Delete all exist Spreadsheets
      Spreadsheet.destroy_all

    #create a session
      @session = GoogleDrive::Session.from_service_account_key("client_secret.json")

    #open spreadsheet
      @spreadsheet = @session.spreadsheet_by_url("https://docs.google.com/spreadsheets/d/1zSVnixUa38vbmbZPgIi2B7Rl304_hFdOBDaSh8osuqs/edit?usp=sharing")

    #create Spreadsheet model
      @spreadsheet.worksheets.each do |list|
        Spreadsheet.create(title: list.title, rows: list.rows)
      end
  end
end
